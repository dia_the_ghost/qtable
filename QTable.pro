#-------------------------------------------------
#
# Project created by QtCreator 2015-07-19T23:25:39
#
#-------------------------------------------------

QT       += core gui

TARGET = QTable
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp

HEADERS  += mainwindow.h

FORMS    += mainwindow.ui
