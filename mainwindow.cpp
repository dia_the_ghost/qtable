#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    ui->tableWidget->setRowCount(8);
    ui->tableWidget->setColumnCount(5);

    ui->tableWidget->setHorizontalHeaderLabels(QStringList() << "a" << "b" << "c" << "d" << "e");
    ui->tableWidget->setVerticalHeaderLabels(QStringList() << "a" << "b" << "c" << "d" << "e" << "f" << "g" << "h");

    //�������� ����� �����

    int count = 1;

    for(int i = 0; i < ui->tableWidget->rowCount(); i++)
    {
        for(int j = 0; j < ui->tableWidget->columnCount(); j++)
        {
            QTableWidgetItem *itm = new QTableWidgetItem(tr("%1").arg(count));

            //itm->setBackgroundColor(Qt::yellow);
            itm->setTextColor(Qt::blue);

            //������ � �������
            ui->tableWidget->setItem(i, j, itm);
            count++;
        }
    }

    QTimer *time = new QTimer(this);
    connect(time, SIGNAL(timeout()), this, SLOT(ActionTimer()));

    time->start(500);

}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::ActionTimer()
{
    srand(time(0));
    int i = rand() % ui->tableWidget->rowCount();
    int j = rand() % ui->tableWidget->columnCount();

    QTableWidgetItem *itm = new QTableWidgetItem("rand");

    itm->setBackgroundColor(Qt::green);
    ui->tableWidget->setItem(i, j, itm);
}


void MainWindow::on_pushButton_clicked()
{
    QTableWidgetItem *itm = ui->tableWidget->currentItem();

    //QMessageBox::information (this, "Info", ui->tableWidget->currentItem()->text());

    QMessageBox::information(this, "Info", itm->text());
}
